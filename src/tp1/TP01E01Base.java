package tp1;

import java.util.Scanner;

public class TP01E01Base {
	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);

		int jour, mois, annee, decalage;
		String moisStr, dateStr;

		System.out.print("Inscrivez une date (JJ MMM AAAA) : ");
		jour = clavier.nextInt();
		moisStr = clavier.next();
		mois = moisEnInt(moisStr);
		annee = clavier.nextInt();
		System.out.print("Inscrivez le decalage : ");
		decalage = clavier.nextInt();

		if (decalage > 0) {
			dateStr = avanceDate(jour, mois, annee, decalage);
		}
		else {
			dateStr = reculeDate(jour, mois, annee, -decalage);
		}
		System.out.print("Nouvelle date : " + dateStr);
		clavier.close();
	}

	public static String moisEnStr(int mois) {
		String moisStr;

		switch (mois) {
			case 1:
				moisStr = "JAN"; 
				break;
			case 2:
				moisStr = "FEV"; 
				break;
			case 3:
				moisStr = "MAR"; 
				break;
			case 4:
				moisStr = "AVR"; 
				break;
			case 5:
				moisStr = "MAI"; 
				break;
			case 6:
				moisStr = "JUN"; 
				break;
			case 7:
				moisStr = "JUL"; 
				break;
			case 8:
				moisStr = "AOU"; 
				break;
			case 9:
				moisStr = "SEP"; 
				break;
			case 10:
				moisStr = "OCT"; 
				break;
			case 11:
				moisStr = "NOV"; 
				break;
			case 12:
				moisStr = "DEC"; 
				break;
			default:
				moisStr = "XXX"; 
				break;
		}
		return moisStr;
	}

	public static int moisEnInt(String moisStr) {
		int mois;

		switch (moisStr) {
			case "JAN":
				mois = 1;
				break;
			case "FEV":
				mois = 2;
				break;
			case "MAR":
				mois = 3;
				break;
			case "AVR":
				mois = 4;
				break;
			case "MAI":
				mois = 5;
				break;
			case "JUN":
				mois = 6;
				break;
			case "JUL":
				mois = 7;
				break;
			case "AOU":
				mois = 8;
				break;
			case "SEP":
				mois = 9;
				break;
			case "OCT":
				mois = 10;
				break;
			case "NOV":
				mois = 11;
				break;
			case "DEC":
				mois = 12;
				break;
		default:
			mois = -1;
			break;
		}
		return mois;
	}

	public static String avanceDate(int jour, int mois, int annee, int decalage) {
		int jourSuivant, moisSuivant, anneeSuivante;

		for (int i = decalage; i > 0; --i) {
			jourSuivant = jourDateSuivante(jour, mois, annee);
			moisSuivant = moisDateSuivante(jour, mois, annee);
			anneeSuivante = anneeDateSuivante(jour, mois, annee);
			jour = jourSuivant;
			mois = moisSuivant;
			annee = anneeSuivante;
		}
		return String.format("%02d %s %d", jour, moisEnStr(mois), annee);
	}

	public static int jourDateSuivante(int jour, int mois, int annee) {
		if (jour == joursParMois(mois, annee)) {
			jour = 1;
		} else {
			jour++;
		}
		return jour;
	}

	public static int moisDateSuivante(int jour, int mois, int annee) {
		if (jour == joursParMois(mois, annee)) {
			mois = (mois % 12) + 1;
		}
		return mois;
	}

	public static int anneeDateSuivante(int jour, int mois, int annee) {
		if (jour == 31 && mois == 12) {
			annee++;
		}
		return annee;
	}

	/**
	 * Retourne le nombre de jours par mois selon le mois et l'ann�e
	 * @param Le mois
	 * @param L'ann�e
	 * @return Le nombre de jours dans le mois par rapport � l'ann�e
	 */
	public static int joursParMois(int mois, int annee) {
		if (mois == 2) { //SI F�VRIER
			if ((annee % 4) == 0 || (annee % 400) == 0) { //SI ANN�E BISEXTILE
				return 29;
			}
			else { //SI ANN�E NON-BISEXTILE
				return 28;
			}
		}
		else if (mois < 8) { //JANVIER A JUILLET
			if (mois % 2 == 0){ //SI MOIS PAIR
				return 30;
			}
			else {
				return 31; //SI MOIS IMPAIR
			}
		}
		else { //AOUT A D�CEMBRE
			if (mois % 2 == 0){ //SI MOIS PAIR
				return 31;
			}
			else {
				return 30; //SI MOIS IMPAIR
			}
		}
	}

	/**
	 * Recule la date avec le d�c�lage sp�cifi� � partir de la date sp�cifi�e
	 * @param Le jour sp�cifi�
	 * @param Le mois sp�cifi�
	 * @param L'ann�e sp�cifi�e
	 * @param Le d�calage sp�cifi�e
	 * @return La date recul�e
	 */
	public static String reculeDate(int jour, int mois, int annee, int decalage) {
		int jourPrecedent, moisPrecedent, anneePrecedent;

		for (int i = decalage; i > 0; --i) {
			jourPrecedent = jourDatePrecedente(jour, mois, annee);
			moisPrecedent = moisDatePrecedente(jour, mois, annee);
			anneePrecedent = anneeDatePrecedente(jour, mois, annee);
			jour = jourPrecedent;
			mois = moisPrecedent;
			annee = anneePrecedent;
		}
		return String.format("%02d %s %d", jour, moisEnStr(mois), annee);
	}

	/**
	 * Retourn�e le jour de la date pr�cendente
	 * @param Le jour
	 * @param Le mois
	 * @param L'ann�e
	 * @return Le jour de la date pr�c�dente
	 */
	public static int jourDatePrecedente(int jour, int mois, int annee) {
		if (jour == 1) { //SI C'EST LE PREMIER JOUR DU MOIS
			if (mois == 1) { //SI C'EST LE DERNIER MOIS DE L'ANN�E
				jour = joursParMois(12, annee);
			}
			else {
				jour = joursParMois(mois - 1, annee);
			}
		} else {
			jour--;
		}
		return jour;
	}

	/**
	 * Retourne le mois de la date pr�c�dente
	 * @param Le jour
	 * @param Le mois
	 * @param L'ann�e
	 * @return Le mois de la date pr�c�dente
	 */
	public static int moisDatePrecedente(int jour, int mois, int annee) {
		if (jour == 1) { //SI C'EST LE PREMIER JOUR DU MOIS
			if (mois == 1) {
				mois = 12;
			}
			else {
				mois--;
			}
		}
		return mois;
	}

	/**
	 * Retourne l'ann�e de la date pr�c�dente
	 * @param Le jour
	 * @param Le mois
	 * @param L'ann�e
	 * @return L'ann�e de la date pr�c�dente
	 */
	public static int anneeDatePrecedente(int jour, int mois, int annee) {
		if (jour == 1 && mois == 1) { //SI C'EST LE PREMIER JOUR DU MOIS ET LE PREMIER MOIS DE L'ANN�E
			annee--;
		}
		return annee;
	}
}
