package tp1;

import static org.junit.Assert.*;

import org.junit.Test;

public class TP01E01BaseTest {

	public static final double EPSILON = 0.0001;
	
	@Test
	public void test() {
		
		//JOUR PAR MOIS TEST
		assertEquals(29, TP01E01Base.joursParMois(2, 2020), EPSILON);
		assertEquals(28, TP01E01Base.joursParMois(2, 2021), EPSILON);
		assertEquals(31,TP01E01Base.joursParMois(3, 2021), EPSILON);
		assertEquals(30, TP01E01Base.joursParMois(4, 2021), EPSILON);
		assertEquals(31, TP01E01Base.joursParMois(12, 2021), EPSILON);
		
		//JOUR DATE PR�C�DENTE TEST
		assertEquals(29, TP01E01Base.jourDatePrecedente(30, 12, 2020), EPSILON);
		assertEquals(30, TP01E01Base.jourDatePrecedente(1, 12, 2020), EPSILON);
		assertEquals(31, TP01E01Base.jourDatePrecedente(1, 11, 2020), EPSILON);
		assertEquals(29, TP01E01Base.jourDatePrecedente(1, 3, 2020), EPSILON);
		assertEquals(28, TP01E01Base.jourDatePrecedente(1, 3, 2021), EPSILON);
		assertEquals(31, TP01E01Base.jourDatePrecedente(1, 1, 2020), EPSILON);
		
		//MOIS DATE PR�C�DENTE TEST
		assertEquals(12, TP01E01Base.moisDatePrecedente(30, 12, 2020), EPSILON);
		assertEquals(11, TP01E01Base.moisDatePrecedente(1, 12, 2020), EPSILON);
		assertEquals(10, TP01E01Base.moisDatePrecedente(1, 11, 2020), EPSILON);
		assertEquals(2, TP01E01Base.moisDatePrecedente(1, 3, 2020), EPSILON);
		assertEquals(12, TP01E01Base.moisDatePrecedente(1, 1, 2021), EPSILON);
		
		//ANN�E DATE PR�C�DENTE TEST
		assertEquals(2020, TP01E01Base.anneeDatePrecedente(30, 12, 2020), EPSILON);
		assertEquals(2020, TP01E01Base.anneeDatePrecedente(1, 12, 2020), EPSILON);
		assertEquals(2020, TP01E01Base.anneeDatePrecedente(1, 11, 2020), EPSILON);
		assertEquals(2020, TP01E01Base.anneeDatePrecedente(1, 3, 2020), EPSILON);
		assertEquals(2020, TP01E01Base.anneeDatePrecedente(1, 1, 2021), EPSILON);
		
		//RECULE DATE TEST
		assertEquals("29 DEC 2020", TP01E01Base.reculeDate(30, 12, 2020, 1));
		assertEquals("31 DEC 2019", TP01E01Base.reculeDate(1, 1, 2020, 1));
		assertEquals("22 JUL 2020", TP01E01Base.reculeDate(30, 10, 2020, 100));
		assertEquals("13 NOV 2015", TP01E01Base.reculeDate(23, 3, 2020, 1592));
		assertEquals("02 JUL 2020", TP01E01Base.reculeDate(31, 11, 2020, 152));
	}

}
